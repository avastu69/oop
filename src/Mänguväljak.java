import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Mänguväljak {
    //tähised: '0' - tühi ruut; 'x' pommiga ruut, '1'-'9' pommide arv ümbritsevates ruutudes
    private int laius = 10;
    private int kõrgus = 10;

    private char[][] mänguväli = new char[10][10];
    private List<List<Integer>> paarid = new ArrayList<>(); //paaride listid, kuhu ei saa pomme asetada(nt esimese käigu positsioon ja positsioonid kus on pommid olemas

    public char[][] getMänguväli() {
        return mänguväli;
    }

    public int getLaius() {
        return laius;
    }

    public int getKõrgus() {
        return kõrgus;
    }

    public List<List<Integer>> getPaarid() {
        return paarid;
    }

    void täidaNullidega() {
        for (char[] rida : mänguväli)
            Arrays.fill(rida, '0');
    }

    void asetaPomm(int x, int y) {//asetab pommi mänguväljale, kus x on tulp, y on rida
        mänguväli[y][x] = 'x';
    }

    void märgiPommiümbrus() { //vaatab tervet mänguvälja, kui kuskil on pomm, ümbritsevatele pommideta ruutudele +1 väärtuse
        for (int i = 0; i < 10; i++) {
            for (int j = 0; j < 10; j++) {

                if (mänguväli[i][j] == 'x') {

                    for (int k = 0; k < 3; k++)
                        for (int l = 0; l < 3; l++) {
                            try {
                                char element = mänguväli[i - 1 + k][j - 1 + l];
                                if (element != 'x')
                                    mänguväli[i - 1 + k][j - 1 + l] = Integer.toString(Character.getNumericValue(element) + 1).charAt(0);
                            }
                            catch (IndexOutOfBoundsException e) {} //kui indeks on listist väljas, ära tee midagi
                        }
                }
            }
        }
    }

    void joonista() { //joonistab mänguvälja
        for (char[] rida : mänguväli) {
            System.out.println(Arrays.toString(rida));
        }
    }

    void kõikPommid(int arv) { //pommide arv on hetkel 10-20; lisab suvaliselt platsile pommid sinna, kuhu saab.
        int pomme = arv;
        int i = 0;
        while (i < pomme) {
            int arv1 = (int)(Math.round(Math.random()*9));
            int arv2 = (int)(Math.round(Math.random()*9));
            List<Integer> arvud = Arrays.asList(arv1, arv2);

            if (!paarid.contains(arvud)){//kui ruut pole juba kasutaja või pommi poolt kasutuses, lisab pommi
                asetaPomm(arv1, arv2);
                paarid.add(arvud);
                i++;
            }
        }
    }



}


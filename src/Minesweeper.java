import javafx.application.Application;
import javafx.application.Platform;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.input.MouseButton;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.StackPane;
import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;
import javafx.scene.text.Font;
import javafx.scene.text.Text;
import javafx.stage.Stage;
import java.awt.*;
import java.io.*;
import java.util.Arrays;
import java.util.Timer;
import java.util.TimerTask;

public class Minesweeper extends Application {

    private Scene stseen;
    private Pane juur = new Pane();
    private Ruut[][] väli = new Ruut[10][10];
    private KasutajaMänguväli kasutajaMänguväli = new KasutajaMänguväli(new Mänguväljak());
    private int käik = 0;
    private int pommide_arv = (int)(Math.round(Math.random()*10+15));
    private boolean võit = false;
    private boolean läbi = false;
    private double ruudulaius = 50;


    private String aeg = "00:00"; //algusaeg
    private Text taimer = new Text(aeg);




    public class Ruut extends StackPane {   //ruudud
        private Rectangle ruut = new Rectangle(ruudulaius, ruudulaius);
        private Text tekst = new Text();


        public Ruut(int x_pos, int y_pos, String väärtus) { //konstruktor
            tekst.setText(väärtus);

            ruut.setFill(Color.DARKGRAY); //loome ruudu ja lisame juurele
            ruut.setStroke(Color.BLACK);
            getChildren().addAll(ruut, tekst);

            setTranslateX(x_pos * ruudulaius);
            setTranslateY(y_pos * ruudulaius + 10);

            setOnMouseClicked(new EventHandler<MouseEvent>() { //kui ruutu vajutada
                @Override
                public void handle(MouseEvent event) {
                    try {
                    MouseButton button = event.getButton(); //kui vasak hiireklõps
                    if(button==MouseButton.PRIMARY && !läbi){
                        if (käik == 0) { //esimesel käigul genereerib mänguvälja
                            for (int i = 0; i < 3; i++) { //esimese vajutuatud ruudu ümber ei tohi pommi olla, et esimene ruut oleks 0
                                for (int j = 0; j < 3; j++) {
                                    try {
                                        kasutajaMänguväli.getMänguväljak().getPaarid().add(Arrays.asList(x_pos-1 + i,y_pos-1 + j)); //lisame kõik esimest ruudu ümbritsevad
                                                                                    //ruudud listi, milles on koordinaadid kus pommi ei tohi olla(esimene ruut lisatakse ka)
                                    }
                                    catch (IndexOutOfBoundsException e){}
                                }
                            }
                            kasutajaMänguväli.getMänguväljak().täidaNullidega();//täidame mittenähtava mänguväljaku nullidega
                            kasutajaMänguväli.getMänguväljak().kõikPommid(pommide_arv);//asetame mänguväljale pommid
                            kasutajaMänguväli.getMänguväljak().märgiPommiümbrus();//märgime pommide ümbruse(ehk lisame pommide ümber olevatele ruutudele +1 väärtuse)
                            käik++;
                        }
                        try {
                        kasutajaMänguväli.valiRuut(x_pos, y_pos, "v"); //vajutab kasutaja poolt valitud ruudule või märgib selle ja uuendab graafilist liidestt
                        uuenda(); } //uuendab kasutajamänguvälja kaudu graafilise liidese väljakut
                        catch (PommiErind e) {throw e;} //kui vajutad pommile, viska erind

                    }else if(button==MouseButton.SECONDARY && !läbi && käik != 0){//kui vasak klõps, siis märgi tabelisse tärn ja uuenda graafikat
                        kasutajaMänguväli.valiRuut(x_pos, y_pos, "m");
                        uuenda();
                    }
                    if (!läbi && käik != 0) { //kui pole esimene käik ja mäng käib, siis kontrollib võitu
                        if (kasutajaMänguväli.võit()) { //võidu puhul avab kõik ruudud ja viskab pommiErindi, kus luuakse uus aken
                            võit = true;
                            läbi = true;
                            for (int i = 0; i < 10; i++) {
                                for (int j = 0; j < 10; j++) {
                                    kasutajaMänguväli.getNähtavVäli()[i][j] = kasutajaMänguväli.getMänguväljak().getMänguväli()[i][j];
                                }
                            }
                            uuenda();
                            timer.cancel();
                            throw new PommiErind();
                        }
                    }

                }
                catch (PommiErind e) { //püüab pommierindi, peatab taimeri ja loob uue akna
                    timer.cancel();
                    GridPane juur2 = new GridPane();
                    juur2.setPadding(new Insets(10,10,10,10)); //lisab akna äärde tühja ruumi
                    juur2.setHgap(5);
                    juur2.setVgap(5);
                    juur2.setPrefSize(120,100);
                    Text mängLäbi;
                    Text mitmes;
                    TextField nimi = new TextField("Sisestage nimi: ");
                    nimi.setPrefColumnCount(20);
                    juur2.add(nimi, 0, 3);

                    if (võit) { //võidu ja kaotuse puhul erinev tagasisidetekst
                        mängLäbi = new Text("Hea töö. Olete võitnud!");
                        mitmes = new Text("Tulemus on pingereas " + mitmesAeg(taimer.getText()) + ". kohal.");
                        juur2.add(mitmes, 0, 2);
                        nimi.setDisable(false);
                    }
                    else {
                        mängLäbi = new Text("Vajutasite pommile. Mäng on läbi!");
                        nimi.setText("Kaotaja tulemus ei loe.");
                        nimi.setDisable(true);
                    }

                    juur2.add(mängLäbi, 0,0);

                    Text aeg = new Text("Teie aeg on: " + taimer.getText());
                    juur2.add(aeg, 0, 1);


                    Button uusMäng = new Button("Uus mäng");
                    juur2.add(uusMäng, 0, 4);
                    Button välju = new Button("Välju");
                    juur2.add(välju, 0, 5);

                    Scene lõpp = new Scene(juur2);
                    Stage lava = new Stage();

                    lava.setScene(lõpp);
                    läbi = true;//läbi = true, et edasi ei saaks mängida
                    lava.show();
                    lava.setResizable(false);

                    uusMäng.setOnMouseClicked(event1 -> { //kui vajutad nuppu "uus mäng", lisab võidu puhul andmed faili ja alustab uue mänguga
                        String nimiSõne = nimi.getText();
                        if (!nimiSõne.equals("") && !nimiSõne.equals("Sisestage nimi: ") && võit) {//kui nime ei sisesta , siis ei lisa tulemust faili
                            try (BufferedWriter bw = new BufferedWriter(new FileWriter("skoorid.txt", true))) {
                                bw.write(nimiSõne + " " + taimer.getText() + System.lineSeparator());
                            } catch (IOException e1) {
                            }
                        }
                        juur = new Pane(); //taastame algsed andmed enne uut mängu
                        võit = false;
                        läbi = false;
                        käik = 0;
                        kasutajaMänguväli = new KasutajaMänguväli(new Mänguväljak()); //loome uue mänguvälja, et mänguväli oleks erinev
                        väli = new Ruut[10][10];
                        HBox box = new HBox(alusta());
                        box.setAlignment(Pos.CENTER);
                        stseen.setRoot(box);
                        timer = new Timer();
                        update();
                        lava.hide();
                    });
                    välju.setOnMouseClicked(event1 -> { //kui kasutaja valib mängust väljuda, lisab võidu puhul andmed faili ja sulgeb programmi
                        String nimiSõne = nimi.getText();
                        if (!nimiSõne.equals("") && !nimiSõne.equals("Sisestage nimi: ") && võit) { //kui sisestatakse tühi nimelahter või muutmata nimelahter, ei tee midagi
                            try (BufferedWriter bw = new BufferedWriter(new FileWriter("skoorid.txt", true))) {
                                bw.write(nimiSõne + " " + taimer.getText() + System.lineSeparator());
                            } catch (IOException e1) {
                            }
                        }
                        Platform.exit(); //väljub täielikult programmist
                    });
                }
                }
            });
        }
    }
    public void uuenda() { //uuendab kasutajamänguvälja kaudu graafilist mänguvälja
        Font font = new Font(20);
        for (int i = 0; i < 10; i++) {
            for (int j = 0; j < 10; j++) {
                String väärtus = Character.toString(kasutajaMänguväli.getNähtavVäli()[i][j]);
                väli[i][j].tekst.setText(väärtus);
                väli[i][j].tekst.setFill(Color.BLACK);
                väli[i][j].tekst.setFont(font);
                if (väärtus.equals("*") || väärtus.equals("x")) //lihtsamaks lugemiseks märgime pommid ja tärnid punasega
                    väli[i][j].tekst.setFill(Color.RED);
            }
        }

    }

    private Parent alusta() { //alustab mängu: lisab taimeri ja kõik ruudud
        taimer.toFront();
        taimer.setX(0);
        taimer.setY(10);
        int käik = 0;


        juur.getChildren().addAll(taimer);
        juur.setPrefSize(500, 510);
        for (int i = 0; i < 10; i++) {
            for (int j = 0; j < 10; j++) {
                Ruut ruut = new Ruut(j, i, " ");
                väli[i][j] = ruut;
                juur.getChildren().addAll(ruut);
            }
        }
        taimer.toFront();
        return juur;
    }

    public String aegSõnena(int sek) { //viib sekundite numbri kujule mm:ss
        String minutidStr = "00";
        String sekundidStr = Integer.toString(sek);
        int minutid = 0;
        int sekundid = sek;
        sekundid += 1;
        if (sekundid >= 60) {
            minutid += 1;
            sekundid -= 60;
        }

        if (sekundid < 10)
            sekundidStr = "0" + sekundid;
        else
            sekundidStr = Integer.toString(sekundid);
        if (minutid < 10)
            minutidStr = "0" + minutid;
        else
            minutidStr = Integer.toString(minutid);
        return minutidStr + ":" + sekundidStr;
    }

    Timer timer = new Timer(); //taimer
    TimerTask task = new TimerTask()
    {   int i = 0;
        public void run()
        {
            aeg = aegSõnena(i);
            taimer.setText(aeg);
            i++;
        }

    };

    public void update() { //taimeri uuendaja, mis alustab uue taimeriga nullist. Toimub iga uue mängu korral
        TimerTask timerTask = new TimerTask() {
            int i = -1;

            @Override
            public void run() {
                aeg = aeg = aegSõnena(i);
                taimer.setText(aeg);
                i++;
            }
        };
        timer.cancel();
        timer = new Timer();
        timer.schedule(timerTask, 1000l, 1000l);
    }


    public int mitmesAeg(String aeg) { //loeb failist mitmes sa edetabelis oled
        int sinustKiirem = 1;
        int sinuaeg = Integer.parseInt(aeg.split(":")[0])*60 + Integer.parseInt(aeg.split(":")[1]);
        try(BufferedReader br = new BufferedReader(new FileReader("skoorid.txt"))) {
            String rida = br.readLine();

            while (rida != null) {
                String[] reaInfo = rida.split(" ");
                String reaAeg = reaInfo[reaInfo.length-1];
                int teiseaeg = Integer.parseInt(reaAeg.split(":")[0])*60 + Integer.parseInt(reaAeg.split(":")[1]);
                if (sinuaeg > teiseaeg)//kui kellelgi aeg on parem kui sinul, viib su järjekorras allapoole
                    sinustKiirem += 1;
                rida = br.readLine();
            }
        }
        catch (IOException e) {}
        return sinustKiirem;
    }


    @Override
    public void start(Stage primaryStage) throws Exception {
        double kõrgus = Toolkit.getDefaultToolkit().getScreenSize().getHeight();
        timer.schedule(task, 1000l, 1000l);//1000l, "l" näitab et arv on long tüüpi
        primaryStage.setMinWidth(515);
        primaryStage.setMinHeight(545);

        HBox kast = new HBox(alusta()); //paneme juure hboxi sisse, et saaks seda tsentreerida
        kast.setAlignment(Pos.CENTER);
        stseen = new Scene(kast);
        stseen.setFill(Color.LIGHTGRAY);



        primaryStage.setTitle("Peaaegu Minesweeper");
        primaryStage.setScene(stseen);
        primaryStage.setMaxHeight(kõrgus);
        primaryStage.setResizable(true);
        primaryStage.show();
    }

    public static void main(String[] args) {
        launch(args);
    }
}

import java.util.Arrays;
import java.util.List;

public class KasutajaMänguväli {

    private char[][] nähtavVäli;
    private char[][] väli;
    private List<Character> numbrid = Arrays.asList('0', '1', '2', '3', '3', '4','5','6','7','8','9');
    private Mänguväljak mänguväljak;

    public KasutajaMänguväli(Mänguväljak mänguväljak) {
        this.mänguväljak = mänguväljak;
        this.väli = mänguväljak.getMänguväli();
        this.nähtavVäli = new char[10][10];
        //paneb vaikeväärusteks tühikud
        for (char[] rida : nähtavVäli)
            Arrays.fill(rida, ' ');//täidab nähtava välja tühikutega

    }


    public Mänguväljak getMänguväljak() {
        return mänguväljak;
    }

    public char[][] getNähtavVäli() {
        return nähtavVäli;
    }

    void joonista() { //kirjutab veerunumbrid ja reanumbri koos reaga
        System.out.println("Veerunumbrid: ");
        System.out.println("   1  2  3  4  5  6  7  8  9 10");
        int i = 1;
        for (char[] rida : nähtavVäli) {
            if (i < 10)
                System.out.println(i + " " + Arrays.toString(rida));
            else
                System.out.println(i + Arrays.toString(rida));
            i++;
        }
    }

    void valiRuut(int x, int y, String märge_vajutus) {//avab nähtavas väljas ruudu
        if (märge_vajutus.equals("m")) { //lisab märke väljale, kui märge on ruudul olemas, eemaldab märke
            if (nähtavVäli[y][x] == '*')
                nähtavVäli[y][x] = ' ';
            else if (!numbrid.contains(nähtavVäli[y][x]))
                nähtavVäli[y][x] = '*';
        }
        else if (märge_vajutus.equals("v")){ //kasutaja vajutab, ehk avab ruudu
            if (nähtavVäli[y][x] != '*') {   //kui valitud ruudul on märge, siis ära muuda midagi
                nähtavVäli[y][x] = väli[y][x];  //avab nähtava välja, ning kontrollib kas vajutasid pommile või mitte.
                if (nähtavVäli[y][x] == 'x')
                    throw new PommiErind(); //Exception lõpetab mängu, kui vajutad pommile
                else if (nähtavVäli[y][x] == '0') //kui vajutasid ruudule 0, avab mäng kõik 0 ümbritsevad ruudud, kus on arv. See toimub rekursiivselt,
                    näitaÜmbrus(x, y);
            }
        }
    }

    void näitaÜmbrus(int x, int y) {//kui ruut on '0', siis avab tema ümber olevad ruudud, mis on arvud(pomme ei ava)
        if (väli[y][x] == '0') {
            for (int k = 0; k < 3; k++)
                for (int l = 0; l < 3; l++) {
                    try {
                        char element = väli[y - 1 + k][x - 1 + l];//ümbritsev element
                        if ((l != 1 || k != 1) && nähtavVäli[y - 1 + k][x - 1 + l] == ' ') { //l != 1 või k != 1 välistavad juhu, et jääme ühte ja sama ruutu kontrollima, sest y-1+1 = y ja x-1+1 = x
                                                                                             // ning avab ainult tühikuga tähistatud(kinnised) ruudud
                            if (numbrid.contains(element)) {    //kui on number, siis lisame ta väljale
                                nähtavVäli[y - 1 + k][x - 1 + l] = element;
                            }
                            if (element == '0')                //kui ümbritsev ruut on 0, siis avame ka selle ümber olevad ruudud
                                näitaÜmbrus(x - 1 + l, y - 1 + k);

                        }
                    } catch (IndexOutOfBoundsException e) { //jällegi väldime indekseid, mis on listist väljas
                    }
                }
        }
    }


    boolean võit() { //kui kõik numbritega ruudud on avatud, tagastab true
        for (int i = 0; i < nähtavVäli.length; i++) {
            for (int j = 0; j < nähtavVäli[0].length; j++) {
                if (numbrid.contains(väli[i][j]) && nähtavVäli[i][j] != väli[i][j])
                    return false;
            }
        }
        return true;
    }



}

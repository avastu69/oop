


//Cristian Noop, Rivo Raudsik ja Aleksander Avastu



import javafx.application.Application;
import javafx.scene.image.Image;
import javafx.stage.Stage;

import java.util.Arrays;
import java.util.Scanner;

public class Mäng extends Application {
    private int pommide_arv = (int)(Math.round(Math.random()*10+25));
    private boolean võit = false;
    private KasutajaMänguväli k;

    public Mäng(KasutajaMänguväli k) {
        this.k = k;
    }

    public boolean isVõit() {
        return võit;
    }

    public void setVõit(boolean võit) {
        this.võit = võit;
    }

    public int getPommide_arv() {
        return pommide_arv;
    }

    public void start(Stage primaryStage) throws Exception {

        KasutajaMänguväli kasutajaMänguväli = new KasutajaMänguväli(new Mänguväljak()); //loome kasutajamänguvälja ja mängu ning arvu i, mis näitab,
                                                                                        // kas tegu on esimese käiguga või ei(i = 0 on esimene käik
        Mäng mäng = new Mäng(kasutajaMänguväli);
        int i = 0;

        while (!mäng.võit) { //kui mäng pole võidetud
            Scanner scanner3 = new Scanner(System.in);
            System.out.println("Kas soovid märkida pommi või avada ruut(Sisestage 'm' või 'v' vastavalt): ");
            String märgeVajutus = scanner3.nextLine();

            Scanner scanner1 = new Scanner(System.in);
            System.out.println("Sisesta rida: ");
            int rida = scanner1.nextInt();

            Scanner scanner2 = new Scanner(System.in);
            System.out.println("Sisesta veerg: ");
            int veerg = scanner2.nextInt();

            if (i == 0) { //esimesel käigul genereerib mänguvälja

                for (int k = 0; k < 3; k++) { //esimese vajutuatud ruudu ümber ei tohi pommi olla, et esimene ruut oleks 0
                    for (int j = 0; j < 3; j++) {
                        try {
                            kasutajaMänguväli.getMänguväljak().getPaarid().add(Arrays.asList(veerg-2 + k,rida - 2 + j)); //lisame kõik esimest ruudu ümbritsevad
                                                                                                //ruudud listi, milles on koordinaadid kus pommi ei tohi olla(esimene ruut lisatakse ka)
                        }
                        catch (IndexOutOfBoundsException e){}
                    }
                }
                kasutajaMänguväli.getMänguväljak().täidaNullidega();//täidame mittenähtava mänguväljaku nullidega
                kasutajaMänguväli.getMänguväljak().kõikPommid(mäng.pommide_arv);//asetame mänguväljale pommid
                kasutajaMänguväli.getMänguväljak().märgiPommiümbrus();//märgime pommide ümbruse(ehk lisame pommide ümber olevatele ruutudele +1 väärtuse)
                i++;
            }
            kasutajaMänguväli.valiRuut(veerg-1, rida-1, märgeVajutus); //vajutab kasutaja poolt valitud ruudule või märgib selle
            mäng.võit = kasutajaMänguväli.võit();//kontrollib kas kasutaja võitis
            kasutajaMänguväli.joonista();   //joonistab kasutajale nähtava mänguvälja
        }
    }

    public static void main(String[] args) {
        launch(args);
    }
}
